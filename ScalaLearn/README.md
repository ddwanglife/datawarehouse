### 开发步骤
1. 新增maven项目，添加src - main 添加scala，并标记为source root
2. 此时默认情况下还不能添加scala文件，需要增加框架，需要IDE安装好
    Add Frameworks Support -> use library -> 添加自己安装的scala
```shell
# 查看环境版本
spark-shell     # version 2.4.6
scala -version  # 2.11.12
hadoop version  # 2.7.7
``` 
### Akka
* JAVA 虚拟机jvm 平台上构建高并发、分布式、容错应用的工具包和运行时，理解成编写并发程序的框架
* 用scala语言写成，同时提供了scala和java的开发接口
* 主要解决的问题：可以轻松的写出高效稳定的并发程序，程序员不再过多的考虑线程、锁、资源竞争等细节

#### actor 模型
    1）处理并发问题关键是要保证共享数据的一致性和正确性，因为程序是多线程时，多个线程对同一个数据
    进行修改，若不加同步条件，势必会造成数据污染。但是当我们对关键代码加入同步条件synchronized
    后，实际上大并发就会阻塞在这段代码，对程序效率有很大影响
    2）若是用单线程处理，不会有数据一致性的问题，但是系统的性能又不能保证
    3）actor模型解决了这个问题，简化并发编程，提升程序性能。actor模型是一种处理并发问题的解决方案。
    
![actor工作机制](./img/actor工作机制.png)

### SparkMLlib
[参考示例](https://www.cnblogs.com/shishanyuan/p/4699644.html)

* 下载scala、spark、hadoop，配置windows运行环境，配置SPARK_HOME、HADOOP_HOME、SCALA_HOME
* 参考博客[连接](https://blog.csdn.net/qq_32653877/article/details/81913648)

    [winutils下载连接](./help/winutils-master.rar)
    
# 代码实战
## 一、JavaWordCount 本地运行spark程序
1. SparkConf conf = new SparkConf().setAppName("JavaWordCount").setMaster("local");
2.  
# Q&A
### 
#### 问题:
```
Exception in thread "main" java.lang.NoClassDefFoundError: scala/Product$class
    at org.apache.spark.SparkConf$DeprecatedConfig.<init>(SparkConf.scala:723)
    at org.apache.spark.SparkConf$.<init>(SparkConf.scala:571)
    at org.apache.spark.SparkConf$.<clinit>(SparkConf.scala)
    at org.apache.spark.SparkConf.set(SparkConf.scala:92)
    at org.apache.spark.SparkConf.set(SparkConf.scala:81)
    at org.apache.spark.SparkConf.setMaster(SparkConf.scala:113)
    at cn.spark.WordCount$.main(WordCount.scala:23)
    at cn.spark.WordCount.main(WordCount.scala)
Caused by: java.lang.ClassNotFoundException: scala.Product$class
    at java.net.URLClassLoader.findClass(URLClassLoader.java:381)
    at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
    at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:335)
    at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
    ... 8 more

Process finished with exit code 1
```
spark 和scala版本不对，spark需要使用2.11.x 版本的scala


### 