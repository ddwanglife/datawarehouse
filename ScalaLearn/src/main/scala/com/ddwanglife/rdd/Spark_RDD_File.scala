package com.ddwanglife.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark_RDD_File {
  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setMaster("local").setAppName("file rdd")
    val sc = new SparkContext(sparkConf)
    val fileRDD: RDD[String] = sc.textFile("ScalaLearn/dataset/fileread.txt")
//    val fileRDD: RDD[String] = sc.textFile("ScalaLearn/dataset/fileread*.txt")  //支持通配符


    println(fileRDD.collect().mkString(","))
    sc.stop()
  }
}
