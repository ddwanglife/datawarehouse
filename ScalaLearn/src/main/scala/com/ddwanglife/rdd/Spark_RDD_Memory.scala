package com.ddwanglife.rdd

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Spark_RDD_Memory {
  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setMaster("local").setAppName("memory rdd")
    val sc = new SparkContext(sparkConf)
    //从内存中创建RDD
    val list = List(1,2,3,4)
//    val rdd: RDD[Int] = sc.parallelize(list)
//    rdd.collect().foreach(println)
//    val rdd = sc.makeRDD(list)
    val rdd = sc.makeRDD(list,4)// 并行数
    println(rdd.collect().mkString(","))
    rdd.saveAsTextFile("ScalaLearn/output")
    sc.stop()
  }
}
