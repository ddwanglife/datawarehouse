package com.ddwanglife.akka.actor

import akka.actor.{Actor, ActorRef, ActorSystem, Props}

class SayHelloActor extends Actor {
  //1.receive 方法会被该Actor的MailBox（实现了Runnable）调用
  //2.当该Actor的MailBox 接收到消息，就会调用receive
  //3.type Receive = PartialFunction[Any,Unit]
  override def receive: Receive = {
      case "hello" => printf("收到hello，回应hello too:)")
      case "ok" => println("收到ok，回应OK too:)")
      case _ => println("匹配不到")
      case "exit" => {
        context.stop(self) //停止actorref
        context.system.terminate()//停止actorsystem
      }

  }
}
object SayHelloActorDemo{
    //1.先创建一个actorsystem,专门用于创建actor
    private val actoryFactory = ActorSystem("actoryFactory")
    //2.创建一个Actor的同时，返回Actor的ActorRef
    // 说明
    // （1）Props[SayHelloActor] 创建了一个SayHelloActor
    // （2）"sayHelloActor" 给actor取名
    // （3）sayHelloActorRef: ActorRef 就是Props[SayHelloActor]的ActorRef
    private val sayHelloActorRef: ActorRef = actoryFactory.actorOf(Props[SayHelloActor],"sayHelloActor")

  def main(args: Array[String]): Unit = {
    //给SayHelloActor 发消息（邮箱）
    sayHelloActorRef ! "hello"

    //如何退出actor模型

  }
}