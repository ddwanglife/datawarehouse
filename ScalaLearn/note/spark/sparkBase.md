# SparkBase

#### 知识点

* spark-shell  (或者spark-shell --master local) 启动

* WebUI 监控页面：http://IP:4040 计算监控页面 8080 Master的资源监控页面

* sc spark 等操作对象，界面已经准备好了，直接可以用

  ```shel
  scala> sc
  res0: org.apache.spark.SparkContext = org.apache.spark.SparkContext@10387d6d
  ```

* 提交应用
  ```shell
  bin/spark-submit \
  --class org.apache.spark.examples.SparkPi \
  --master local[2] \
  ./examples/jars/spark-examples_2.11-2.4.6.jar \
  10
  
  # --class 执行程序的主类
  # --master local[2] 部署模式为本地模式，数字表示分配的虚拟机CPU核数量  local spark yarn
  # spark-example  运行的应用类所在的jar包
  # 10 表示程序的入口参数，用于设定当前应用的任务数量
  ```

* standalone模式 常见示例一个Master（8080资源监控） 三个worker
  ```shell
  bin/spark-submit \
  --class org.apache.spark.examples.SparkPi \
  --master spark://ip(或者主机名):7077 \
  ./examples/jars/spark-examples_2.11-2.4.6.jar \
  10
  
  # standalone HA 需要启动并配置zookeeper集群、选择另外一个服务器单独启动master进程
  # --master spark://ip1:7077,ip2:7077
  # 两个master 一个是alive 一个是standby，状态不一样
  # 增加 --deploy cluster
  ```
  
* Yarn 模式
  ```shell
  bin/spark-submit \
  --class org.apache.spark.examples.SparkPi \
  --master yarn \
  ./examples/jars/spark-examples_2.11-2.4.6.jar \
  10
  # 必须在spark_env.sh 配置YARN_CONF_DIR路径，${HADOOP_HOME}/etc/hadoop
  ```
  ```
  错误分析参考 ： https://blog.csdn.net/yu0_zhang0/article/details/80090902
  
  错误示例：
20/07/23 18:09:29 ERROR spark.SparkContext: Error initializing SparkContext.
org.apache.hadoop.ipc.RemoteException(org.apache.hadoop.hdfs.server.namenode.SafeModeException): Cannot create directory /user/root/.sparkStaging/application_1595498951166_0001. Name node is in safe mode.
  
  
  产生的原因：Name node is in safe mode
  这是因为在分布式文件系统启动的时候，开始的时候会有安全模式，当分布式文件系统处于安全模式的情况下，文件系统中的内容不允许修改也不允许删除，直到安全模式结束。安全模式主要是为了系统启动的时候检查各个DataNode上数据块的有效性，同时根据策略必要的复制或者删除部分数据块。运行期通过命令也可以进入安全模式。在实践过程中，系统启动的时候去修改和删除文件也会有安全模式不允许修改的出错提示，只需要等待一会儿即可。
  可以通过以下命令来手动离开安全模式：
  1. $HADOOP_HOME/bin/hadoop dfsadmin -safemode leave ;
  2. 用户可以通过dfsadmin -safemode value 来操作安全模式，参数value的说明如下：
  
  enter - 进入安全模式
  leave - 强制NameNode离开安全模式
  get - 返回安全模式是否开启的信息
  wait - 等待，一直到安全模式结束。
  ```
  ```
  错误分析参考：https://blog.csdn.net/clever_wr/article/details/77092754
  错误示例：20/07/24 09:06:54 ERROR cluster.YarnClientSchedulerBackend: YARN application has exited unexpectedly with state UNDEFINED! Check the YARN application logs for more details.
  
  当jdk版本是1.8时常常会出现这个问题，就直接改hadoop中yarn-site.xml的配置
  <property>
      <name>yarn.nodemanager.pmem-check-enabled</name>
      <value>false</value>
  </property>
  <property>
      <name>yarn.nodemanager.vmem-check-enabled</name>
      <value>false</value>
  </property>
  之后保存，重新格式化，重启进程即可
  ```
  
* 端口号
  ```
Spark查看当前Spark-shell 运行任务情况端口号：4040（计算）
SparkMaster内部通信服务端口号：7077
Standalone模式下，SparkMasterWeb端口号：8080（资源）
Spark历史服务端口号：18080
Hadoop Yarn 运行任务情况查看端口号：8088
  ```
```
  
* 核心对象
  Driver Executor Master&Worker ApplicationMaster

* RDD：弹性分布式数据集 累加器：分布式共享只写变量 广播变量：分布式共享只读变量

### 案例实操
统计出每一个省份每个广告被点击数量排行的TOP3
```