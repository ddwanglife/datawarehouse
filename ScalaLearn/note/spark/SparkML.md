MLlib 是Apache Spark组件之一，专注机器学习，支持Scala、Python、Java 和 R语言

优点：

1，扩展性好，不是单机

2，性能高，集群

3，文档api，社区都完善

缺点：

1，算法少。现在也越来越完善



Mahout  基于MR，算法很多

#### 两个算法包
1. spark.mllib : 包含原始API，构建在RDD之上
2. spark.ml：基于DataFrame构建的高级API（DataFrame DataSet 是之后的趋势）
#### 如何选择
1. spark.ml 具备更优的性能和更好的扩展性，建议优先选用；
2. spark.mllib 仍将继续更新，且目前包含更多（相比于spark.ml）的算法
#### 数据类型
向量，带类别的向量(vector)，矩阵(Matrix)等
#### 数据统计计算库
基本统计量（min max average等-扩展：也有基于概率） ，相关分析，随机数产生器，假设检验等
#### 机器学习管道（pipeline）
Transformer/Estimator/Parameter
#### 机器学习算法（参照scikit-learn）
* 分类算法
* 回归算法
* 聚类算法
* 协同过滤

#### 参照深度实践Spark机器学习
1. [下载学习数据](http://files.grouplens.org/datasets/movielens/ml-100k.zip)

2. 创建hdfs目录：hadoop fs -mkdir /u01     /u01/bigdata/data

3. 上传文件：hadoop fs -put u.user /u01/bigdata/data

   hadoop fs -ls /u01/bigdata/data
4. 